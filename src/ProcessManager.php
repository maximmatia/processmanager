<?php

namespace ProcessManager;

class ProcessManager
{
    protected $myPid;

    public function __construct()
    {
        $this->myPid = getmypid();
    }

    /**
     * @return int
     */
    public function getMyPid(): int
    {
        return $this->myPid;
    }

    public function getPid($process, $isStrict = true): array
    {
        $s = $isStrict ? '$' : '';
        exec("ps -o pid,command -ax | grep '{$process}{$s}' | grep -v 'grep' | awk '{print $1}'", $p);

        return $p;
    }

    public function getProcessList($process, array $options = [], $isStrict = false): array
    {
        if (sizeof($options) > 0) {
            $o = '-o ' . implode(',', $options);
        } else {
            $o = '';
        }

        $s = $isStrict ? '$' : '';
        exec("ps {$o} -ax | grep '{$process}{$s}' | grep -v 'grep'", $p);

        return $p;
    }

    public function isExists($process, $excludeMe = true): bool
    {
        $p = $this->getPid($process, true);
        if ($excludeMe) {
            $p = array_diff_key(array_flip($p), [$this->getMyPid() => true]);
        }

        if (sizeof($p) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isMe($process): bool
    {
        return array_key_exists($this->getMyPid(), array_flip($this->getPid($process, true)));
    }

    public function isMeOnly($process, $attempts = 3): bool
    {
        for ($i = 0; $i < $attempts; $i++) {
            $p = $this->getPid($process, true);
            $r = array_key_exists($this->getMyPid(), array_flip($p)) AND sizeof($p) === 1;
            if ($r) {
                return $r;
            } elseif ($i + 1 < $attempts) {
                usleep(10000);
            }
        }

        return false;
    }

    public function killPid($pid)
    {
        if (is_array($pid)) {
            $pid = implode(' ', $pid);
        }
        exec('kill ' . $pid);
    }

    public function killPidForced($pid)
    {
        if (is_array($pid)) {
            $pid = implode(' ', $pid);
        }

        exec('kill -9 ' . $pid);
    }

    public function killProcess($process, $forced = false)
    {
        $p = $this->getPid($process);
        if ($forced) {
            $this->killPidForced($p);
        } else {
            $this->killPid($p);
        }
    }

    public function exec($process): array
    {
        exec($process, $o);

        return $o;
    }

    public function getLoad()
    {
        $uptime = exec('uptime');
        if (preg_match('!load average: ([\d\.]+), [\d\.]+, [\d\.]+!si', $uptime, $cpu)) {
            return (float)str_replace(',', '.', $cpu[1]);
        }

        return 0;
    }

    public function killRepetitiveProcesses($pattern)
    {
        $pl = $this->getProcessList($pattern, ['pid', 'etimes', 'command']);

        $kill = $base = [];
        foreach ($pl as $item) {
            preg_match('/(\d+)\s*(\d+)\s*/', $item, $matches);
            if ($matches[1] and $matches[2] > 0) {
                $p = trim(str_replace($matches[0], '', $item));
                if (!array_key_exists($p, $base)) {
                    $base[$p] = ['pid' => $matches[1], 'lifetime' => $matches[2]];
                } elseif ($base[$p]['lifetime'] > $matches[2]) {
                    $kill[] = $matches[1];
                } else {
                    $kill[]   = $base[$p]['pid'];
                    $base[$p] = ['pid' => $matches[1], 'lifetime' => $matches[2]];
                }
            }
        }

        if (sizeof($kill) > 0) {
            $this->killPid(implode(' ', $kill));
        }
    }

}
